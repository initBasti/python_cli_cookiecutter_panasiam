#!/usr/bin/env python3

from {{cookiecutter.project_slug}}.cli import main

if __name__ == '__main__':
    main()
