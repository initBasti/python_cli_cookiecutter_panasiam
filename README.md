# Cookiecutter Template for Python Projects

This cookiecutter represents a template for simple python CLI applications.

This template uses [Poetry](https://python-poetry.org/) as a dependency manager.

## Usage

1. Install [Cookiecutter](https://github.com/cookiecutter/cookiecutter) globally
    ```bash
    python3 -m pip install cookiecutter
    ```
2. Use my template
    ```bash
    cookiecutter https://gitlab.com/initBasti/python_cli_cookiecutter
    cd <project-folder>
    git init
    poetry install
    git add .
    git commit -m "Initial commit"
    ```
